const express = require('express')
const groupRouter = require('./group/group.routes')
const itemRouter = require('./item/item.routes')
const userRouter = require('./user/user.routes')
const port = process.env.PORT;

require('./db/db')

const app = express()

app.use(express.json())
app.use(groupRouter)
app.use(itemRouter)
app.use(userRouter)

app.listen(port, () => {
    console.log(`Server running on port ${port}`)
})