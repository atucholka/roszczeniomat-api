const express = require('express')
const Group = require('./group.model')
const auth = require('../middleware/auth')

const router = express.Router()

router.post('/group/addMember', auth, async (req, res) => {
    try {
        const { userId, groupId } = req.body;

        if (!userId || !groupId) {
            return res.status(400).json({ msg: "Not all fields have been entered.", code: 1 });
        }

        const foundGroup = await Group.findOne({ _id: groupId });
        if (!Object.keys(foundGroup).length) {
            return res.status(400).json({ msg: "Group not found.", code: 2 });
        }
        if (foundGroup.members.includes(userId)) {
            return res.status(400).json({ msg: "Member already added.", code: 3 });
        }
        foundGroup.members = [...foundGroup.members, userId];
        await foundGroup.save()
        res.status(201).send({ foundGroup })
    } catch (error) {
        res.status(400).send(error)
    }
})

router.post('/group/deleteMember', auth, async (req, res) => {
    try {
        const { userId, groupId } = req.body;

        if (!userId || !groupId) {
            return res.status(400).json({ msg: "Not all fields have been entered.", code: 1 });
        }

        const foundGroup = await Group.findOne({ _id: groupId });
        if (!Object.keys(foundGroup).length) {
            return res.status(400).json({ msg: "Group not found.", code: 2 });
        }
        if (foundGroup.members.includes(userId)) {
            return res.status(400).json({ msg: "Member already added.", code: 3 });
        }
        foundGroup.members = foundGroup.members.filter(memberId => memberId !== userId);
        await foundGroup.save()
        res.status(201).send({ foundGroup })
    } catch (error) {
        res.status(400).send(error)
    }
})

module.exports = router