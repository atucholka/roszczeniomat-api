const mongoose = require('mongoose')

const groupSchema = mongoose.Schema({
    name: {
        type: String,
        trim: true
    },
    authorId: {
        type: String
    },
    members: {
        type: Array
    },
    creationDate: {
        type: String
    },
    isPublic: {
        type: Boolean
    }
});


const Group = mongoose.model('Group', groupSchema)

module.exports = Group