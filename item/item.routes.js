const express = require('express')
const Item = require('./item.model')
const User = require('./../user/user.model')
const auth = require('../middleware/auth')

const router = express.Router()

router.post('/item/add', auth, async (req, res) => {
    try {
        if (!req.body) {
            return res.status(400).send({ error: 'No values passed', code: 'ITEM_ADD_NO_VALUES' })
        }
        if (!req.body.name || !req.body.wantingRate || !req.body.authorId || !req.body.beneficientId) {
            return res.status(400).send({ error: 'Values missing', code: 'ITEM_ADD_VALUES_MISSING' })
        }
        const foundItem = await Item.findOne({
            authorId: req.body.authorId,
            $or: [{ name: req.body.name }, { url: req.body.url }]
        });
        if (foundItem.name === req.body.name) {
            return res.status(400).send({ error: 'Item with this name already added', code: 'ITEM_ADD_ALREADY_ADDED_NAME' })
        }
        if (foundItem.url === req.body.url) {
            return res.status(400).send({ error: 'Item with this url already added', code: 'ITEM_ADD_ALREADY_ADDED_URL' })
        }

        const item = new Item(req.body)
        await item.save();
        res.status(201).send({ item })
    } catch (error) {
        res.status(400).send(error);
    }
});

router.post('/item/delete/:id', auth, async (req, res) => {
    try {
        if (!req.params.id) {
            return res.status(400).send({ error: 'Item id missing', code: 'ITEM_DELETE_ID_MISSING' })
        }
        const foundItem = await Item.findOne({
            _id: req.params.id
        });
        if (!foundItem) {
            return res.status(400).send({ error: 'No such item found', code: 'ITEM_DELETE_NOT_FOUND' })
        }

        await foundItem.remove();
        res.status(200).send({})
    } catch (error) {
        res.status(400).send(error);
    }
});

router.get('/item/:id', auth, async (req, res) => {
    try {
        if (!req.params.id) {
            return res.status(400).send({ error: 'No item id passed', code: 'ITEM_GET_ID_MISSING' })
        }
        const foundItem = await Item.findOne({
            _id: req.params.id
        });
        if (!foundItem) {
            return res.status(400).send({ error: 'No such item found', code: 'ITEM_DELETE_NOT_FOUND' })
        }

        res.status(200).send(foundItem)
    } catch (error) {
        res.status(400).send(error);
    }
});

router.post('/item/edit', auth, async (req, res) => {
    try {
        if (!req.body) {
            return res.status(400).send({ error: 'No values passed', code: 'ITEM_EDIT_NO_VALUES' })
        }
        if (!req.body.itemId || !req.body.authorId || !req.body.itemEditedFields) {
            return res.status(400).send({ error: 'Values missing', code: 'ITEM_EDIT_VALUES_MISSING' })
        }

        const foundItem = await Item.findOne({
            _id: req.body.itemId
        });

        if (!foundItem) {
            return res.status(400).send({ error: 'Item not found.', code: 'ITEM_EDIT_ITEM_NOT_FOUND' })
        }
        if (foundItem.authorId !== req.body.authorId && foundItem.beneficientId !== req.body.authorId) {
            return res.status(400).send({ error: 'Only author or beneficient can edit item.', code: 'ITEM_EDIT_EDITOR_NOT_AUTHOR_NOR_BENEFICIENT' })
        }

        Object.keys(req.body.itemEditedFields).forEach(key => {
            foundItem[key] = req.body.itemEditedFields[key]
        });

        await foundItem.save();
        res.status(201).send({ foundItem })
    } catch (error) {
        res.status(400).send(error);
    }
});

router.post('/item/addBenefactor', auth, async (req, res) => {
    try {
        if (!req.body) {
            return res.status(400).send({ error: 'No values passed', code: 'ITEM_ADD_BENEFACTOR_NO_VALUES' })
        }
        if (!req.body.itemId) {
            return res.status(400).send({ error: 'Item id missing.', code: 'ITEM_ADD_BENEFACTOR_ITEM_ID_MISSING' })
        }
        if (!req.body.benefactorId) {
            return res.status(400).send({ error: 'Benefactor missing.', code: 'ITEM_ADD_BENEFACTOR_BENEFACTOR_ID_MISSING' })
        }

        const foundBenefactor = await User.find({ _id: req.body.benefactorId });

        if (!foundBenefactor) {
            return res.status(400).send({ error: 'No such user found.', code: 'ITEM_ADD_BENEFACTOR_BENEFACTOR_NOT_FOUND' })
        }
        const foundItem = await Item.findOne({
            _id: req.body.itemId
        });
        if (foundItem.benefactorsId.includes(req.body.benefactorId)) {
            return res.status(400).send({ error: 'Benefactor already added.', code: 'ITEM_ADD_BENEFACTOR_BENEFACTOR_ALREADY_ADDED' })
        }

        if (!foundItem) {
            return res.status(400).send({ error: 'No such item found.', code: 'ITEM_ADD_BENEFACTOR_ITEM_NOT_FOUND' })
        }

        if (req.body.benefactorId.includes(foundItem.authorId)) {
            return res.status(400).send({ error: 'Author cannot be his own benefactor.', code: 'ITEM_ADD_BENEFACTOR_EQUAL_TO_AUTHOR' })
        }

        foundItem.benefactorsId = [...foundItem.benefactorsId, req.body.benefactorId];
        await foundItem.save();
        res.status(200).send({ foundItem })
    } catch (error) {
        res.status(400).send(error);
    }
});

router.post('/item/deleteBenefactor', auth, async (req, res) => {
    try {
        if (!req.body) {
            return res.status(400).send({ error: 'No values passed', code: 'ITEM_DELETE_BENEFACTOR_NO_VALUES' })
        }
        if (!req.body.itemId) {
            return res.status(400).send({ error: 'Item id missing.', code: 'ITEM_DELETE_BENEFACTOR_ITEM_ID_MISSING' })
        }
        if (!req.body.benefactorId) {
            return res.status(400).send({ error: 'Benefactor missing.', code: 'ITEM_DELETE_BENEFACTOR_BENEFACTOR_ID_MISSING' })
        }

        const foundBenefactor = await User.find({ _id: req.body.benefactorId });

        if (!foundBenefactor) {
            return res.status(400).send({ error: 'No such user found.', code: 'ITEM_DELETE_BENEFACTOR_BENEFACTOR_NOT_FOUND' })
        }
        const foundItem = await Item.findOne({
            _id: req.body.itemId
        });
        if (!foundItem.benefactorsId.includes(req.body.benefactorId)) {
            return res.status(400).send({ error: 'Benefactor was not a benefactor before.', code: 'ITEM_DELETE_BENEFACTOR_BENEFACTOR_NOT_BEING_BENEFACTOR_BEFORE' })
        }

        if (!foundItem) {
            return res.status(400).send({ error: 'No such item found.', code: 'ITEM_DELETE_BENEFACTOR_ITEM_NOT_FOUND' })
        }

        foundItem.benefactorsId = foundItem.benefactorsId.filter(existingBenefactor => existingBenefactor !== req.body.benefactorId);
        await foundItem.save();
        res.status(200).send({ foundItem })
    } catch (error) {
        res.status(400).send(error);
    }
});

module.exports = router;