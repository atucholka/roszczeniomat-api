const mongoose = require('mongoose')

const itemSchema = mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    url: {
        type: String,
        trim: true
    },
    comment: {
        type: String,
        trim: true
    },
    wantingRate: {
        type: Number,
        required: true
    },
    groups: {
        type: Array,
    },
    lists: {
        type: Array,
    },
    authorId: {
        type: String,
        required: true
    },
    beneficientId: {
        type: String,
        required: true
    },
    benefactorsId: {
        type: Array,
    },
    price: {
        type: Number
    },
    creationDate: {
        type: Date
    },
    lastEditionDate: {
        type: Date
    },
});


const Item = mongoose.model('Item', itemSchema)

module.exports = Item